﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.ComponentModel.DataAnnotations;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using XploRe.AspNetCore.Mvc.ModelBinding;
using XploRe.Runtime;


namespace XploRe.AspNetCore.Mvc.TagHelpers
{

    /// <inheritdoc />
    /// <summary>
    ///     Appends the short display name of a model property expression specified by the <c>xn-short-display-name-for</c>
    ///     attribute to the end of the tag. The tag will always be rendered with a separate start and end tag.
    /// </summary>
    [HtmlTargetElement("*", Attributes = ShortDisplayNameForAttributeName)]
    public class ShortDisplayNameHelper : TagHelper
    {

        private const string ShortDisplayNameForAttributeName = "xn-short-display-name-for";


        #region Attributes 

        /// <summary>
        ///     Name of current model property to retrieve the short display name for.
        /// </summary>
        [CanBeNull]
        [HtmlAttributeName(ShortDisplayNameForAttributeName)]
        public ModelExpression For { get; set; }

        #endregion


        /// <inheritdoc />
        /// <remarks>Does nothing is <see cref="For" /> is <c>null</c>.</remarks>
        public override void Process([NotNull] TagHelperContext context, [NotNull] TagHelperOutput output)
        {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            if (output == null) {
                throw new ArgumentNullException(nameof(output));
            }

            if (For == null) {
                return;
            }

            if (For.Metadata == null) {
                throw new RuntimeInconsistencyException(
                    "{0}.{1} is null.".FormatWith(nameof(For), nameof(For.Metadata))
                );
            }

            var displayAttribute = For.Metadata.GetPropertyAttribute<DisplayAttribute>();
            var displayName = displayAttribute?.ShortName;

            if (!string.IsNullOrEmpty(displayName)) {
                output.PostContent?.SetContent(displayName);
            }

            output.TagMode = TagMode.StartTagAndEndTag;
        }

    }

}
