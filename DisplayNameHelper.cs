﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;


namespace XploRe.AspNetCore.Mvc.TagHelpers
{

    /// <inheritdoc />
    /// <summary>
    ///     Adds the display name of a model property expression specified by the <c>xn-display-name-for</c> attribute
    ///     to the beginning of the tag. The tag will always be rendered with a separate start and end tag.
    /// </summary>
    [HtmlTargetElement("*", Attributes = DisplayNameForAttributeName)]
    public class DisplayNameHelper : TagHelper
    {

        private const string DisplayNameForAttributeName = "xn-display-name-for";


        #region Attributes 

        /// <summary>
        ///     Name of current model property to retrieve the display name for.
        /// </summary>
        [CanBeNull]
        [HtmlAttributeName(DisplayNameForAttributeName)]
        public ModelExpression For { get; set; }

        #endregion


        /// <inheritdoc />
        /// <remarks>Does nothing if <see cref="For" /> is <c>null</c>.</remarks>
        public override void Process([NotNull] TagHelperContext context, [NotNull] TagHelperOutput output)
        {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            if (output == null) {
                throw new ArgumentNullException(nameof(output));
            }

            if (For == null) {
                return;
            }

            var displayName = For.Metadata?.DisplayName;

            if (!string.IsNullOrEmpty(displayName)) {
                output.PreContent?.SetContent(displayName);
            }

            output.TagMode = TagMode.StartTagAndEndTag;
        }

    }

}
