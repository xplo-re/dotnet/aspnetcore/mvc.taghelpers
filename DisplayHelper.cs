﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using XploRe.Runtime;


namespace XploRe.AspNetCore.Mvc.TagHelpers
{

    /// <inheritdoc />
    /// <summary>
    ///     Adds the display markup of a model property expression specified by the <c>xn-display-for</c> attribute to
    ///     the beginning of the tag. The tag will always be rendered with a separate start and end tag.
    /// </summary>
    [HtmlTargetElement("*", Attributes = DisplayForAttributeName)]
    public class DisplayHelper : TagHelper
    {

        private const string DisplayForAttributeName = "xn-display-for";


        #region Attributes 

        /// <summary>
        ///     Name of current model property to retrieve the display markup for.
        /// </summary>
        [CanBeNull]
        [HtmlAttributeName(DisplayForAttributeName)]
        public ModelExpression For { get; set; }

        #endregion


        /// <summary>
        ///     The automatically assigned <see cref="ViewContext" /> instance.
        /// </summary>
        [HtmlAttributeNotBound]
        [ViewContext]
        public ViewContext ViewContext { get; set; }

        /// <inheritdoc />
        /// <remarks>Does nothing if <see cref="For" /> is <c>null</c>.</remarks>
        public override void Process([NotNull] TagHelperContext context, [NotNull] TagHelperOutput output)
        {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            if (output == null) {
                throw new ArgumentNullException(nameof(output));
            }

            if (For == null) {
                return;
            }

            if (ViewContext == null) {
                throw new RuntimeInconsistencyException(
                    "{0}.{1} is null.".FormatWith(typeof(TagHelper), nameof(ViewContext))
                );
            }

            // Contextualise the HTML helper for use within this instance.
            // Cf. https://github.com/aspnet/Mvc/issues/5504
            (HtmlHelper as IViewContextAware)?.Contextualize(ViewContext);

            // Format expression equivalently to @Html.DisplayFor(For) in a view.
            var displayValue = HtmlHelper.Display(For.Name);

            if (displayValue != null) {
                output.PreContent?.SetHtmlContent(displayValue);
            }

            output.TagMode = TagMode.StartTagAndEndTag;
        }


        #region (Services)

        /// <summary>
        ///     The cached <see cref="IHtmlHelper" /> service instance.
        /// </summary>
        protected IHtmlHelper HtmlHelper { get; }

        /// <summary>
        ///     Initialises a new <see cref="DisplayHelper" /> instance.
        /// </summary>
        /// <param name="htmlHelper">The <see cref="IHtmlHelper" /> instance retrieved via DI.</param>
        public DisplayHelper(IHtmlHelper htmlHelper)
        {
            HtmlHelper = htmlHelper;
        }

        #endregion

    }

}
