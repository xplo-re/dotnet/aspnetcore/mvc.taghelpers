﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;
using XploRe.Runtime;


namespace XploRe.AspNetCore.Mvc.TagHelpers
{

    /// <inheritdoc />
    /// <summary>
    ///     Authorises the <see cref="T:System.Security.Claims.ClaimsPrincipal" /> in the view's 
    ///     <see cref="T:Microsoft.AspNetCore.Http.HttpContext" /> against policies, claims and roles. If either 
    ///     authorisation fails, the HTML element is suppressed.
    /// </summary>
    [HtmlTargetElement("*", Attributes = AuthorizePolicyAttributeName)]
    public class AuthorizeHelper : TagHelper
    {

        private const string AuthorizePolicyAttributeName = "xn-authorize-policy";


        #region Attributes

        /// <summary>
        ///     Policy to authorise user against if set. Can either be an <see cref="AuthorizationPolicy" /> instance or
        ///     the name of a policy to check. The name of the policy must have been registered, otherwise an
        ///     <see cref="InvalidOperationException" /> is thrown when evaluating the policy.
        /// </summary>
        [CanBeNull]
        [HtmlAttributeName(AuthorizePolicyAttributeName)]
        public object Policy { get; set; }

        #endregion


        /// <summary>
        ///     The automatically assigned <see cref="ViewContext" /> instance.
        /// </summary>
        [HtmlAttributeNotBound]
        [ViewContext]
        public ViewContext ViewContext { get; set; }

        /// <inheritdoc />
        public override async Task ProcessAsync([NotNull] TagHelperContext context, [NotNull] TagHelperOutput output)
        {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            if (output == null) {
                throw new ArgumentNullException(nameof(output));
            }

            if (Policy == null) {
                return;
            }

            if (ViewContext == null) {
                throw new RuntimeInconsistencyException(
                    "{0}.{1} is null.".FormatWith(typeof(TagHelper), nameof(ViewContext))
                );
            }

            var user = ViewContext.HttpContext?.User;

            // Try to resolve policy expression.
            var authorized = false;

            if (Policy is AuthorizationPolicy policy) {
                var authorizeAsync = AuthorizationService.AuthorizeAsync(user, policy);

                if (authorizeAsync != null) {
#if NETSTANDARD2_0
                    authorized = (await authorizeAsync)?.Succeeded == true;
#else
                    authorized = await authorizeAsync;
#endif
                }
            }
            else {
                var authorizeAsync = AuthorizationService.AuthorizeAsync(user, (string) Policy);

                if (authorizeAsync != null) {
#if NETSTANDARD2_0
                    authorized = (await authorizeAsync)?.Succeeded == true;
#else
                    authorized = await authorizeAsync;
#endif
                }
            }

            if (!authorized) {
                output.SuppressOutput();
            }
        }


        #region (Services)

        /// <summary>
        ///     The cached <see cref="IAuthorizationService" /> service instance.
        /// </summary>
        [NotNull]
        protected IAuthorizationService AuthorizationService { get; }

        /// <summary>
        ///     Initialises a new <see cref="AuthorizeHelper" /> instance.
        /// </summary>
        /// <param name="authorizationService">
        ///     The <see cref="IAuthorizationService" /> instance retrieved via DI.
        /// </param>
        public AuthorizeHelper([NotNull] IAuthorizationService authorizationService)
        {
            if (authorizationService == null) {
                throw new ArgumentNullException(nameof(authorizationService));
            }

            AuthorizationService = authorizationService;
        }

        #endregion

    }

}
