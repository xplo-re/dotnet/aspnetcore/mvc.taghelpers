﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Microsoft.AspNetCore.Razor.TagHelpers;
using XploRe.Runtime;


namespace XploRe.AspNetCore.Mvc.TagHelpers
{

    /// <inheritdoc />
    /// <summary>
    ///     Adds CSS class(es) defined by the attribute <c>xn-validation-error-class</c> to a tag if validation failed
    ///     for the model field identified by the expression in attribute <c>xn-validation-for</c>.
    /// </summary>
    [HtmlTargetElement("*", Attributes = ValidationForAttributeName + "," + ValidationErrorClassAttributeName)]
    public class ValidationClassHelper : TagHelper
    {

        private const string ValidationForAttributeName = "xn-validation-for";
        private const string ValidationErrorClassAttributeName = "xn-validation-error-class";


        #region Attributes 

        /// <summary>
        ///     Name to be validated on the current model. If not specified, no action is performed.
        /// </summary>
        [CanBeNull]
        [HtmlAttributeName(ValidationForAttributeName)]
        public ModelExpression For { get; set; }

        /// <summary>
        ///     CSS class(es) to add to the tag in case validation failed.
        /// </summary>
        [CanBeNull]
        [HtmlAttributeName(ValidationErrorClassAttributeName)]
        public string ErrorClass { get; set; }

        #endregion


        /// <summary>
        ///     The automatically assigned <see cref="ViewContext" /> instance.
        /// </summary>
        [HtmlAttributeNotBound]
        [ViewContext]
        public ViewContext ViewContext { get; set; }

        /// <inheritdoc />
        /// <remarks>Does nothing if <see cref="For" /> is <c>null</c>.</remarks>
        public override void Process([NotNull] TagHelperContext context, [NotNull] TagHelperOutput output)
        {
            if (context == null) {
                throw new ArgumentNullException(nameof(context));
            }

            if (output == null) {
                throw new ArgumentNullException(nameof(output));
            }

            if (For == null) {
                return;
            }

            // Try to resolve For expression.
            var fullName = NameAndIdProvider.GetFullHtmlFieldName(ViewContext, For.Name);

            if (string.IsNullOrEmpty(fullName)) {
                throw new ArgumentException(
                    $"Field name given by '{ValidationForAttributeName}' cannot be null or empty"
                );
            }

            if (ViewContext == null) {
                throw new RuntimeInconsistencyException(
                    "{0}.{1} is null.".FormatWith(typeof(TagHelper), nameof(ViewContext))
                );
            }

            ModelStateEntry entry = null;
            ViewContext.ViewData?.ModelState?.TryGetValue(fullName, out entry);

            if (entry?.Errors?.Any() != true) {
                // No validation error found.
                return;
            }

            // Append tag with given CSS class(es) by creating a virtual tag and merging it into the existing tag.
            var tag = new TagBuilder(output.TagName);
            tag.AddCssClass(ErrorClass);

            output.MergeAttributes(tag);
        }

    }

}
